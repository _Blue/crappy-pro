from django.test import TestCase, Client
from .models import Quote
import json

class ApiTests(TestCase):
    def setUp(self):
        self.quote = Quote.objects.create(author="Author", content="Content", title="Title")
        self.client = Client()

    def test_get_quote(self):
        response = self.client.get("/quotes/%i" % self.quote.id)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content.decode(), json.dumps(self.quote.as_json()))

    def test_list_quotes(self):
        response = self.client.get("/quotes")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content.decode(), json.dumps([self.quote.as_json()]))

    def test_list_2_quotes(self):
        self.quote2 = Quote.objects.create(author="Author2", content="Content2", title="Title2")
        self.quote2.save()

        response = self.client.get("/quotes")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content.decode(), json.dumps([self.quote.as_json(), self.quote2.as_json()]))

    def test_get_quote_bad_id(self):
        response = self.client.get("/quotes/1337")

        self.assertEqual(response.status_code, 404)

    def test_publish_quote_bad_id(self):
        response = self.client.post("/quotes/publish/1337")

        self.assertEqual(response.status_code, 404)

    def test_publish_quote_correct_id_twice(self):
        self.assertIsNone(self.quote.publication_date)
        self.assertEqual(self.quote.state, Quote.PENDING_APPROVAL)

        response = self.client.post("/quotes/%i/publish" % self.quote.id)

        def check(response, code=200):
            self.quote.refresh_from_db()
            self.assertEqual(response.status_code, code)
            self.assertIsNotNone(self.quote.publication_date)
            self.assertEqual(self.quote.state, Quote.PUBLISHED)
            self.assertEqual(response.content.decode(), json.dumps(self.quote.as_json()))

        check(response)
        response = self.client.post("/quotes/%i/publish" % self.quote.id)
        check(response, 202)

    def test_publish_quote_correct_id(self):
        self.assertIsNone(self.quote.publication_date)
        self.assertEqual(self.quote.state, Quote.PENDING_APPROVAL)

        response = self.client.post("/quotes/%i/publish" % self.quote.id)

        self.quote.refresh_from_db()
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(self.quote.publication_date)
        self.assertEqual(self.quote.state, Quote.PUBLISHED)
        self.assertEqual(response.content.decode(), json.dumps(self.quote.as_json()))

