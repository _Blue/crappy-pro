from datetime import datetime
from http import HTTPStatus

from django.http import JsonResponse
from django.shortcuts import *
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from crappypro.models import Quote


class api(View):
    def list(request):
        quotes = [quote.as_json() for quote in Quote.objects.all()]
        return JsonResponse(quotes, safe=False)

    def details(request, id: int):
        quote = get_object_or_404(Quote, id=id)

        return JsonResponse(quote.as_json())

    @csrf_exempt
    def create(request):
        content = request.POST['content']
        author = request.POST['author']
        title = request.POST['title']

        new_quote = Quote(author=author, content=content, title=title)
        new_quote.save()

        return JsonResponse(new_quote.as_json())

    def publish(request, id: int):
        quote = get_object_or_404(Quote, id=id)
        if quote.publication_date:
            return JsonResponse(quote.as_json(), status=HTTPStatus.ACCEPTED)

        quote.publication_date = datetime.now()
        quote.state = Quote.PUBLISHED
        quote.save()

        return JsonResponse(quote.as_json())
