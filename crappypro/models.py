import json
from django.db import models

class Quote(models.Model):
    PENDING_APPROVAL = 0
    PUBLISHED = 1
    HIDDEN = 2

    STATE_MAP = (
            (PENDING_APPROVAL, 'Pending approoval'),
            (PUBLISHED, 'Published'),
            (HIDDEN, 'Hidden')
            )

    author = models.CharField(max_length=32)
    title = models.CharField(max_length=32)
    content = models.CharField(max_length=1024)
    state = models.IntegerField(default=PENDING_APPROVAL, choices=STATE_MAP)
    creation_date = models.DateTimeField('Date created', auto_now_add=True)
    publication_date = models.DateTimeField('Date published', null=True, blank=True, default=None)

    def as_json(self) -> dict:
        return {
                    'author': self.author,
                    'content': self.content,
                    'state': self.state,
                    'creation_date': int(self.creation_date.timestamp()),
                    'publication_date': int(self.creation_date.timestamp())
                  }
