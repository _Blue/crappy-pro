import time
from django.http import Http404

def id_as_int_or_404(id: str) -> int:
    try:
        return int(id)
    except:
        raise Http404

def date_to_ts(date) -> int:
    return time.mktime(date.timetuple())
